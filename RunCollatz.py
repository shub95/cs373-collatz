#!/usr/bin/env python3

# -------------
# RunCollatz.py
# -------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = too-many-lines

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve, collatz_calculate


# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
